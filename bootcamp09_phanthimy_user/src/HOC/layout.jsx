import { Box, Typography } from '@material-ui/core'
import React from 'react'
import Header from '../components/header/index'
export default function Layout(props) {
    return (
        <Box>
            <Box>
            <Header></Header>
            </Box>
            {props.children}
            <Box>
            <Typography component="h1" variant="h5" color="secondary" align="center" style={{background:"#3f51b5"}}>FOOTER</Typography>
            </Box>
        </Box>
        
    )
}
