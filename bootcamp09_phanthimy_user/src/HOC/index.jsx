import React from "react";
import { Redirect, Route } from "react-router-dom";
 function CreateRoute(condition) {
  return (props) => {
    const { path, Component, redirectPath, exact } = props;
    return (
      <Route
        path={path}
        exact={exact}
        render={() => {
          if (condition()) {
            return <Component />;
          }
          return <Redirect to={redirectPath} />;
        }}
      ></Route>
    );
  };
}
export const AuthRoute = CreateRoute(()=>localStorage.getItem("t"))
