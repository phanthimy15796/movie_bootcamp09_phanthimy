import axios from "axios";

export const request =({ url,method,data,params})=>{
    const variables={
        url,
        method,
        data,
        params,
    }
    const token= localStorage.getItem("t");
    if(token){
        variables.headers={
            Authorization :"Bearer " + token,
        };
    }
    return axios (variables);
};