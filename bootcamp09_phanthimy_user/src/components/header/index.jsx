import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import styles from "./style"
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import { NavLink } from 'react-router-dom';
import { Box, Button } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { createAction } from '../../store/actions/createAction';
import { types } from '../../store/actions/types';



 function Header() {
  const classes = styles();
  const dispatch = useDispatch();
 const handleLogOut =()=>{
   localStorage.removeItem("t");
   dispatch(createAction(types.SET_LOGOUT,""));
 };
 const getMe = useSelector((state)=>{
   return state.me.infoMe;
 })
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="open drawer"
          >
            <MenuIcon />
          </IconButton>
          <Typography className={classes.title} variant="h6" noWrap>
            MOVIES
          </Typography>
          <Box>
              <NavLink className={classes.navLink} activeClassName={classes.active} to="/" exact >Home</NavLink>
              <NavLink className={classes.navLink} activeClassName={classes.active} to="/profile">Profile</NavLink>
              <Button onClick={handleLogOut}>Log Out</Button>
          </Box>
          {getMe && <Typography> Hi!{getMe.hoTen}</Typography>}
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
export default Header;