import {
  Box,
  Button,
  Container,
  TextField,
  Typography,
} from "@material-ui/core";
import { useFormik } from "formik";
import React from "react";
import { useDispatch } from "react-redux";
import { fetchMe } from "../../store/actions/me";

export default function Signin() {
  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
    },
  });
  const dispatch = useDispatch();
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formik.values);
    dispatch(fetchMe(formik.values));
  };

  return (
    <div>
      <Container>
        <form onSubmit={handleSubmit}>
          <Box marginBottom="15px">
            <Typography gutterBottom>Account</Typography>
            <TextField
              onChange={formik.handleChange}
              name="taiKhoan"
              id="outlined-basic"
              label=""
              variant="outlined"
              fullWidth
              size="small"
              placeholder="Account"
            />
          </Box>
          <Box marginBottom="15px">
            <Typography>Password</Typography>
            <TextField
              type="password"
              id="outlined-basic"
              label=""
              variant="outlined"
              fullWidth
              size="small"
              placeholder="Password"
              name="matKhau"
              onChange={formik.handleChange}
            />
          </Box>
          <Button type="submit" variant="outlined" color="secondary">
            Sign In
          </Button>
        </form>
      </Container>
    </div>
  );
}
