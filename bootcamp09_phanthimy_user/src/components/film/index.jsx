import React from "react";
import styles from "./style";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { NavLink } from "react-router-dom";

export default function Film(props) {
  const classes = styles();
  const { tenPhim, hinhAnh, moTa,maPhim } = props.movies;
 
  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt="Contemplative Reptile"
          height="140"
          image={hinhAnh}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography
            gutterBottom
            variant="h5"
            component="h2"
            style={{ height: 60 }}
          >
            {tenPhim.substr(0, 15)}
          </Typography>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            style={{ height: 60 }}
          >
            {moTa.substr(0, 50)}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <NavLink to={"/detail/"+maPhim } >
          <Button size="small" color="primary">
            Detail
          </Button>
        </NavLink>

        <Button size="small" color="primary">
          Tailer
        </Button>
      </CardActions>
    </Card>
  );
}
