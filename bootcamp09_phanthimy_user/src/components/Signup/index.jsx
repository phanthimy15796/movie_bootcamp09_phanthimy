import {
  Box,
  Button,
  Container,
  TextField,
  Typography,
} from "@material-ui/core";
import axios from "axios";
import { useFormik } from "formik";
import React from "react";
import { useCallback } from "react";
import * as yup from "yup";

let schema = yup.object().shape({
  hoTen: yup.string().required("name is invalid"),
  taiKhoan: yup.string().required("account is invalid"),
  matKhau: yup.string().required("password is invalid"),
  email: yup.string().required("email is invalid ").email("email is not true"),
  soDT: yup
    .string()
    .required("phone number is invalid ")
    .matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g, "number is invalid"),
});
export default function Signup() {
  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDT: "",
      maNhom: "GP01",
      hoTen: "",
    },
    validationSchema: schema,
    validateOnMount: true,
  });
  const setAllTouch = useCallback(() => {
    Object.keys(formik.values).forEach((item) => {
      formik.setFieldTouched(item);
    });
  }, [formik]);
  const handleSubmit = (e) => {
    e.preventDefault();
    setAllTouch();
    if (!formik.isValid) return;
    axios({
      url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/DangKy",
      method: "POST",
      data: formik.values,
    })
      .then((res) => {
        console.log(res);
        alert("đăng kí thành công");
        formik.resetForm();
      })
      .catch((err) => {
        const errMess = { ...err };
        alert(errMess.response.data.content);
      });
  };

  return (
    <div>
      <Container>
        <form onSubmit={handleSubmit}>
          <Box marginBottom="15px">
            <Typography>Name</Typography>
            <TextField
              id="outlined-basic"
              label=""
              variant="outlined"
              fullWidth
              size="small"
              placeholder="Name"
              onChange={formik.handleChange}
              name="hoTen"
              onBlur={formik.handleBlur}
              value={formik.values.hoTen}
            />
            <Box color="#dd1515">
              {formik.touched.hoTen && formik.errors.hoTen}
            </Box>
          </Box>
          <Box marginBottom="15px">
            <Typography>Account</Typography>
            <TextField
              id="outlined-basic"
              label=""
              variant="outlined"
              fullWidth
              size="small"
              placeholder="Account"
              onChange={formik.handleChange}
              name="taiKhoan"
              onBlur={formik.handleBlur}
              value={formik.values.taiKhoan}
            />
            <Box color="#dd1515">
              {formik.touched.taiKhoan && formik.errors.taiKhoan}
            </Box>
          </Box>
          <Box marginBottom="15px">
            <Typography>Password</Typography>
            <TextField
              type="Password"
              id="outlined-basic"
              label=""
              variant="outlined"
              fullWidth
              size="small"
              placeholder="Password"
              onChange={formik.handleChange}
              name="matKhau"
              onBlur={formik.handleBlur}
              value={formik.values.matKhau}
            />
            <Box color="#dd1515">
              {formik.touched.matKhau && formik.errors.matKhau}
            </Box>
          </Box>
          <Box marginBottom="15px">
            <Typography>Email</Typography>
            <TextField
              id="outlined-basic"
              label="Email"
              variant="outlined"
              fullWidth
              size="small"
              placeholder="Email"
              onChange={formik.handleChange}
              name="email"
              onBlur={formik.handleBlur}
              value={formik.values.email}
            />
            <Box color="#dd1515">
              {formik.touched.email && formik.errors.email}
            </Box>
          </Box>
          <Box marginBottom="15px">
            <Typography>Phone number</Typography>
            <TextField
              id="outlined-basic"
              label="Phone Number"
              variant="outlined"
              fullWidth
              size="small"
              placeholder="Phone number"
              onChange={formik.handleChange}
              name="soDT"
              onBlur={formik.handleBlur}
              value={formik.values.soDT}
            />
            <Box color="#dd1515">
              {formik.touched.soDT && formik.errors.soDT}
            </Box>
          </Box>
          <Button type="submit" variant="outlined" color="secondary">
            Sign Up
          </Button>
        </form>
      </Container>
    </div>
  );
}
