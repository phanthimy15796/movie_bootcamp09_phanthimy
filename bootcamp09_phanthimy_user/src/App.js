import Profile from "./views/Profile";
import Detail from "./views/Detail";
import Home from "./views/Home";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import {getMe} from './store/actions/me'
import { AuthRoute } from "./HOC";

function App() {
  const dispatch = useDispatch();
 useEffect(()=>{
    dispatch(getMe);
 },[dispatch]);
  return (
    <BrowserRouter>
      
      <Switch>
        <AuthRoute path="/profile" exact Component={Profile} redirectPath="/"></AuthRoute>
        <AuthRoute path="/detail/:id" exact Component={Detail} redirectPath="/"></AuthRoute>
        <Route path="/" exact component={Home}></Route>
      </Switch>
    </BrowserRouter>
  );

}

export default App;
