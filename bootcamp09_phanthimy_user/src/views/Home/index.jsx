import { Box, Container, Grid, Typography } from "@material-ui/core";
import React, { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Film from "../../components/film";
import { fetchMovies } from "../../store/actions/movies";
import makeStyles from "./styles";
import Pagination from "@material-ui/lab/Pagination";
import Layout from "../../HOC/layout";
import Popup from "../../components/popup";

export default function Home() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchMovies());
  }, [dispatch]);
  const movieList = useSelector((state) => {
    return state.movies.movieList;
  });
  const classes = makeStyles();
  const [page, setPage] = React.useState(1);
  const handleChange = useCallback(
    (event, value) => {
      setPage(value);

      dispatch(fetchMovies(value));
    },
    [dispatch]
  );

  return (
    <Layout>
      <Box position="fixed" right="10px" top="15%" zIndex="99" boxShadow="#ccc 0 0 10px">
        <Popup></Popup>
      </Box> 
      <Container maxWidth="md">
        <Typography variant="h4" align="center" color="primary">
          MOVIES LIST
        </Typography>
        <Grid container spacing={3} style={{ marginTop: 30 }}>
          {movieList.items &&
            movieList.items.map((item) => {
              return (
                <Grid key={item.maPhim} item md={3}>
                  <Film movies={item}></Film>
                </Grid>
              );
            })}
        </Grid>
        <Box
          className={classes.root}
          display="flex"
          justifyContent="center"
          width="100%"
        >
          <Typography>Page: {page}</Typography>
          <Pagination
            count={movieList.totalPages}
            page={page}
            onChange={handleChange}
          />
        </Box>
      </Container>
    </Layout>
  );
}
