import {
  Avatar,
  Box,
  Container,
  Grid,
  InputBase,
  Typography,
} from "@material-ui/core";
import React from "react";
import Layout from "../../HOC/layout";
import makeStyles from "./style";
import cls from "classnames";
import { useSelector } from "react-redux";
export default function Profile() {
  const info = useSelector((state) => {
    return state.me.infoMe;
  });
  const classes = makeStyles();
  return (
    <Layout>
      <Typography variant="h4" color="secondary" align="center" gutterBottom>
        MY PROFILE
      </Typography>

      <Container maxWidth="sm">
        <Box borderRadius="5px" boxShadow="#ccc 0 0 10px" padding="15px">
          <Grid container>
            <Grid item md={5}>
              <Avatar
                className={cls(classes.orange, classes.large)}
                alt="Remy Sharp"
                src="https://picsum.photos/200"
              />
            </Grid>
            <Grid item md={7}>
              <Box>
                <Box marginBottom="15px">
                  <Typography>Account :</Typography>
                  <InputBase
                    value={info.taiKhoan}
                    disabled
                    fullWidth
                  ></InputBase>
                </Box>
                <Box marginBottom="15px">
                  <Typography>Password :</Typography>
                  <InputBase value={info.matKhau} fullWidth></InputBase>
                </Box>
                <Box marginBottom="15px">
                  <Typography>Name :</Typography>
                  <InputBase value={info.hoTen} fullWidth></InputBase>
                </Box>
                <Box marginBottom="15px">
                  <Typography>Email :</Typography>
                  <InputBase value={info.email} fullWidth></InputBase>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Layout>
  );
}
