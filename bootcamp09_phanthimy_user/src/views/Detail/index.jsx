import { Box, CardMedia, Container, Grid, Typography } from "@material-ui/core";
import React from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouteMatch } from "react-router-dom";
import Layout from "../../HOC/layout";
import { fetchDetail } from "../../store/actions/movies";
export default function Detail() {
  const match = useRouteMatch();
  console.log(match);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchDetail(match.params.id));
  }, [match,dispatch]);
  const movieDetail = useSelector((state)=>{
      return state.movies.movieDetail;
  });
  const {hinhAnh,moTa} = movieDetail || {};
  return (
    <Layout>
      <Container maxWidth="md" style={{ marginTop: 20 }}>
        <Grid container spacing={3}>
          <Grid item md={6}>
            <CardMedia
              component="img"
              alt="Contemplative Reptile"
              height="140"
              image={hinhAnh}
              title="Contemplative Reptile"
            />
          </Grid>
          <Grid item md={6}>
            <Typography>{moTa}</Typography>
          </Grid>
        </Grid>
      </Container>
    </Layout>
  );
}
