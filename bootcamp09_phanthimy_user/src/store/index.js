import {combineReducers,createStore,applyMiddleware,compose} from 'redux'
import thunk from "redux-thunk"
import movies from './reducers/movies'
import me from './reducers/me'
const rootReducer = combineReducers({
    movies,
    me,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunk)),
);
export default store;
 