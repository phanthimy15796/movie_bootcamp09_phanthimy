import { types } from "../actions/types";

const initialState = {
    movieList :[],
    movieDetail:null
};

const store = (state = initialState, {type,payload})=>{
    switch (type) {
        case types.FETCH_MOVIES:
            state.movieList = payload;
            return {...state};
        case types.FETCH_DETAIL:
            state.movieDetail = payload;
            return {...state};
        default:
            return state;
    };
};
export default store;