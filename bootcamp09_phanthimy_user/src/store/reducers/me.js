import { types } from "../actions/types";

const initiaState ={
    infoMe:"",
};
const store= (state=initiaState,{type,payload})=>{
 switch(type){
     case types.FETCH_ME:
         state.infoMe = payload;
         return {...state};
    case types.SET_LOGOUT:
        state.infoMe = payload;
        return {...state};
    default :
    return state;
 };
};
 export default store;