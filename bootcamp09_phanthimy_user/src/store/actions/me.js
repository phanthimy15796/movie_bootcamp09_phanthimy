import axios from 'axios';
import { request } from '../../api/request';
import { createAction } from './createAction';
import { types } from './types';
export const fetchMe=(info)=>{
    return async (dispatch)=>{
        try{
            const res = await request({
                url:"http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/DangNhap",
                method:"POST",
                data:info,
            })
            console.log(res);
            dispatch(createAction(types.FETCH_ME,res.data.content));
            localStorage.setItem("t",res.data.content.accessToken)
        }catch(err){
            console.log(err);
        }
    }
}
export const getMe =async(dispatch)=>{
    try{
        const res= await request({
           url:"http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/ThongTinTaiKhoan", 
           method:"POST",
           headers:{
            Authorization :"Bearer " + localStorage.getItem("t"),
           }
        })
        console.log(res);
        dispatch(createAction(types.FETCH_ME,res.data.content));
    }catch(err){
        console.log(err);
    }
}