import axios from "axios";
import { request } from "../../api/request";
import { createAction } from "./createAction";
import { types } from "./types";
export const fetchMovies = (id = 1) => {
  return async (dispatch) => {
    try {
      const res = await request({
        url: "http://movieapi.cyberlearn.vn/api/QuanLyPhim/LayDanhSachPhimPhanTrang",
        method: "GET",
        params: {
          maNhom: "GP01",
          soTrang: id,
          soPhanTuTrenTrang: 12,
        },
      });
      console.log(res);
      dispatch(createAction(types.FETCH_MOVIES, res.data.content));
    } catch (err) {
      console.log(err);
    }
  };
};
export const fetchDetail = (code) => {
  return async (dispatch) => {
    try {
      const res = await request({
        url: "http://movieapi.cyberlearn.vn/api/QuanLyPhim/LayThongTinPhim",
        method: "GET",
        params: {
          MaPhim: code,
        },
      });
      console.log(res);
      dispatch(createAction(types.FETCH_DETAIL, res.data.content))
    } catch (err) {
      console.log(err);
    }
  };
};
