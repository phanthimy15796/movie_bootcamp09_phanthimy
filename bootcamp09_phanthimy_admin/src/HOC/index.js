import { Route, Redirect } from "react-router-dom";
import React from "react";

export default function CreateRouter(condition) {
  return (props) => {
    const { path, Component, exact, Redirectpath } = props;
    return (
      <Route
        path={path}
        exact={exact}
        render={() => {
          if (condition()) {
            return <Component />;
          }
          return <Redirect to={Redirectpath} />;
        }}
      ></Route>
    );
  };
}
export const AuthRoute = CreateRouter(() => !localStorage.getItem("t"));
export const AuthPrivate  = CreateRouter(()=> localStorage.getItem("t"));
