import { types } from "../actions/types";

const initialState = {
  admin: "",
  listUser:"",
};
const store = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.FETCH_AD:
      state.admin = payload;
      return { ...state };
    case types.FETCH_LIST_USER:
      state.listUser = payload;
      return {...state};
    default:
      return state;
  }
};
export default store;
