import { request } from "../../api/request";
import { createActions } from "./createActions";
import { types } from "./types";

export const signinAdmin = (info, callBack) => {
  return async (dispatch) => {
    try {
      const res = await request({
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/DangNhap",
        method: "POST",
        data: info,
      });
      console.log(res);
      if (res.data.content.maLoaiNguoiDung === "QuanTri") {
        dispatch(createActions(types.FETCH_AD, res.data.content));
        localStorage.setItem("t", res.data.content.accessToken);
        callBack();
      } else {
        alert("you have not power to access");
      }
    } catch (err) {
      console.log(err);
    }
  };
};
export const getAd = async (dispatch) => {
  try {
    const res = await request({
      url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/ThongTinTaiKhoan",
      method: "POST",
    });
    console.log(res);
    dispatch(createActions(types.FETCH_AD, res.data.content));
  } catch (err) {
    console.log(err);
  }
};
export const deleteUser = (id) => {
  return async (dispatch) => {
    try {
      const res = await request({
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/XoaNguoiDung",
        method: "DELETE",
        params: {
          TaiKhoan: id,
        },
      });
      console.log(res);
      alert("success delete");
    } catch (err) {
      console.log(err);
    }
  };
};
