
import { request } from "../../api/request";
import { createActions } from "./createActions";
import { types } from "./types";
export const fetchListUser = async (dispatch) => {
  try {
    const res = await request({
      url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDung",
      method: "GET",
      params: {
        MaNhom: "GP01",
      },
    }); 
    console.log(res);
    
    dispatch(createActions(types.FETCH_LIST_USER, res.data.content));
  } catch (err) {
    console.log(err);
  }
};
