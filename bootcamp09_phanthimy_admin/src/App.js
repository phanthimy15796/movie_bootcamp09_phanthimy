import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import { AuthPrivate, AuthRoute } from "./HOC";
import { getAd } from "./store/actions/admin";
import DashBroad from "./views/Dashbroad";
import Signin from "./views/Signin";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAd);
  }, [dispatch]);

  return (
    <BrowserRouter>
      <Switch>
        <AuthRoute path="/" exact Component={Signin} Redirectpath='/dashbroad'/>
        <AuthPrivate path="/dashbroad" exact Component={DashBroad} Redirectpath='/' />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
