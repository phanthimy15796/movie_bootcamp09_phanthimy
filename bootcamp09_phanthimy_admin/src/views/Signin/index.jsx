import {
  Box,
  Button,
  Container,
  TextField,
  Typography,
} from "@material-ui/core";
import { useFormik } from "formik";
import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { signinAdmin } from "../../store/actions/admin";

export default function Signin() {
  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
    },
  });
  
  
  const dispatch =useDispatch();
  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(signinAdmin(formik.values,goDashBroad));
  };
  const history = useHistory();
  const goDashBroad =()=>{
    history.push("/dashbroad");
  }
  
  return (
    <Container maxWidth="sm">
      <Typography gutterBottom color="primary" variant="h3" align="center">
        SIGN IN
      </Typography>
      <form onSubmit={handleSubmit}>
        <Box marginBottom="15px">
          <Typography gutterBottom>Account</Typography>
          <TextField
            onChange={formik.handleChange}
            name="taiKhoan"
            id="outlined-basic"
            label=""
            variant="outlined"
            fullWidth
            size="small"
            placeholder="Account"
            value={formik.values.taiKhoan}
          />
        </Box>
        <Box marginBottom="15px">
          <Typography>Password</Typography>
          <TextField
            type="password"
            id="outlined-basic"
            label=""
            variant="outlined"
            fullWidth
            size="small"
            placeholder="Password"
            name="matKhau"
            onChange={formik.handleChange}
            value={formik.values.matKhau}
          />
        </Box>
        <Button type="submit" variant="outlined" color="primary" >
          Sign In
        </Button>
      </form>
    </Container>
  );
}
