import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "contents",
    marginLeft: theme.spacing(2.5),
  },
  table: {
    minWidth: 700,
  },
  type: {
    color: "#000",
    fontSize: "50px",
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    width: "100%",
  },
}));
export default useStyles;
