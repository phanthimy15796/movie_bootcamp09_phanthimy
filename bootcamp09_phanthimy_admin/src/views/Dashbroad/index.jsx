import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useTheme, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import {
  Box,
  Button,
  Container,
  FormControl,
  Grid,
  MenuItem,
  TableHead,
  TextField,
  Typography,
} from "@material-ui/core";
import useStyles from "./style";
import { useDispatch, useSelector } from "react-redux";
import { fetchListUser } from "../../store/actions/listuser";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import { useFormik } from "formik";
import * as yup from "yup";
import { useCallback } from "react";
import { deleteUser } from "../../store/actions/admin";
import { request } from "../../api/request";

function TablePaginationActions(props) {
  const classes = useStyles();
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};
const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);
let schema = yup.object().shape({
  hoTen: yup.string().required("name is invalid"),
  taiKhoan: yup.string().required("account is invalid"),
  matKhau: yup.string().required("password is invalid"),
  email: yup.string().required("email is invalid ").email("email is not true"),
  soDT: yup
    .string()
    .required("phone number is invalid ")
    .matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g, "number is invalid"),
});

export default function DashBroad() {
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(6);
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDt: "",
      maNhom: "GP01",
      maLoaiNguoiDung: "",
      hoTen: "",
    },
    validationSchema: schema,
    validateOnMount: true,
  });
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formik.values);
    setAllTouch();
    if (!formik.values) return;

    if (!update) {
      request({
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/ThemNguoiDung",
        method: "POST",
        data: formik.values,
      })
        .then((res) => {
          console.log(res);
          alert("success add");
        })
        .catch((err) => {
          console.log(err);
        });
      dispatch(fetchListUser);
    } else {
      request({
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung",
        method: "POST",
        data: formik.values,
      })
        .then((res) => {
          console.log(res);
          alert("success update");
        })
        .catch((err) => {
          console.log(err);
        });
      dispatch(fetchListUser);
      setUpdate(false);
    }
    formik.resetForm();
    
  };
  const setAllTouch = useCallback(() => {
    Object.keys(formik.values).forEach((item) => {
      formik.setFieldTouched(item);
    });
  }, [formik]);

  useEffect(() => {
    dispatch(fetchListUser);
  }, [dispatch]);
  const listUser = useSelector((state) => {
    return state.infoAdmin.listUser;
  });

  const handleDelete = (userName) => {
    dispatch(deleteUser(userName));
    dispatch(fetchListUser);
    console.log(userName);
  };
  const [update, setUpdate] = useState(false);
  const handleUpdate = (item) => {
    formik.setValues({
      taiKhoan: item.taiKhoan,
      matKhau: item.matKhau,
      email: item.email,
      soDt: item.soDt,
      maLoaiNguoiDung: item.maLoaiNguoiDung,
      hoTen: item.hoTen,
      maNhom:"GP01",
    });

    setUpdate(true);
  };
  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, listUser.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <div>
      <Typography variant="h3" color="secondary" align="center" gutterBottom>
        DASHBROAD
      </Typography>
      <Container maxWidth="md">
        <Box boxShadow="#ccc 0 0 10px" padding="20px">
          <form onSubmit={handleSubmit}>
            <Grid container spacing={5}>
              <Grid item md={6}>
                <Box marginBottom="15px">
                  <Typography gutterBottom>Account</Typography>
                  {!update ?  <TextField
                  
                    name="taiKhoan"
                    variant="outlined"
                    fullWidth
                    size="small"
                    placeholder="Account"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.taiKhoan}
                  /> :  <TextField
                  disabled
                  name="taiKhoan"
                  variant="outlined"
                  fullWidth
                  size="small"
                  placeholder="Account"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.taiKhoan}
                />}
                 
                </Box>
                <Box color="#dd1515">
                  {formik.touched.taiKhoan && formik.errors.taiKhoan}
                </Box>
                <Box marginBottom="15px">
                  <Typography gutterBottom>Password</Typography>
                  <TextField
                    type="password"
                    name="matKhau"
                    onChange={formik.handleChange}
                    variant="outlined"
                    fullWidth
                    size="small"
                    placeholder="Password"
                    onBlur={formik.handleBlur}
                    value={formik.values.matKhau}
                  />
                </Box>
                <Box color="#dd1515">
                  {formik.touched.matKhau && formik.errors.matKhau}
                </Box>
                <Box marginBottom="15px">
                  <Typography gutterBottom>Type</Typography>
                  <FormControl className={classes.formControl}>
                    <InputLabel id="demo-simple-select-label"></InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      name="maLoaiNguoiDung"
                      onChange={formik.handleChange}
                      value={formik.values.maLoaiNguoiDung}
                    >
                      <MenuItem value="KhachHang">KhachHang</MenuItem>
                      <MenuItem value="QuanTri">QuanTri</MenuItem>
                    </Select>
                  </FormControl>
                </Box>
              </Grid>
              <Grid item md={6}>
                <Box marginBottom="15px">
                  <Typography gutterBottom>Email</Typography>
                  <TextField
                    name="email"
                    onChange={formik.handleChange}
                    variant="outlined"
                    fullWidth
                    size="small"
                    placeholder="Email"
                    onBlur={formik.handleBlur}
                    value={formik.values.email}
                  />
                </Box>
                <Box color="#dd1515">
                  {formik.touched.email && formik.errors.email}
                </Box>
                <Box marginBottom="15px">
                  <Typography gutterBottom>Phone Number</Typography>
                  <TextField
                    name="soDt"
                    onChange={formik.handleChange}
                    variant="outlined"
                    fullWidth
                    size="small"
                    placeholder="Phone Number"
                    onBlur={formik.handleBlur}
                    value={formik.values.soDt}
                  />
                </Box>
                <Box color="#dd1515">
                  {formik.touched.soDt && formik.errors.soDt}
                </Box>
                <Box marginBottom="15px">
                  <Typography gutterBottom>Name</Typography>
                  <TextField
                    name="hoTen"
                    onChange={formik.handleChange}
                    variant="outlined"
                    fullWidth
                    size="small"
                    placeholder="Name"
                    onBlur={formik.handleBlur}
                    value={formik.values.hoTen}
                  />
                </Box>
                <Box color="#dd1515">
                  {formik.touched.hoTen && formik.errors.hoTen}
                </Box>
              </Grid>
            </Grid>
            <Box display="block" textAlign="center">
              {!update ? (
                <Button type="submit" variant="outlined" color="secondary">
                  ADD
                </Button>
              ) : (
                <Button type="submit" variant="outlined" color="secondary">
                  UPDATE
                </Button>
              )}
            </Box>
          </form>
        </Box>
        <Box marginTop="30px">
          <Typography variant="h5" color="primary" align="center" gutterBottom>
            LIST ADMIN
          </Typography>
          <TableContainer>
            <Table
              className={classes.table}
              aria-label="custom pagination table"
            >
              <TableHead>
                <TableRow>
                  <StyledTableCell>Name</StyledTableCell>
                  <StyledTableCell align="right">Account</StyledTableCell>
                  <StyledTableCell align="right">Type</StyledTableCell>
                  <StyledTableCell align="right">Delete</StyledTableCell>
                  <StyledTableCell align="right">Update</StyledTableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {listUser &&
                  (rowsPerPage > 0
                    ? listUser.slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                    : listUser
                  ).map((item) => (
                    <StyledTableRow key={item.taiKhoan}>
                      <StyledTableCell component="th" scope="row">
                        {item.hoTen}
                      </StyledTableCell>
                      <StyledTableCell style={{ width: 160 }} align="right">
                        {item.taiKhoan}
                      </StyledTableCell>
                      <StyledTableCell style={{ width: 160 }} align="right">
                        {item.maLoaiNguoiDung}
                      </StyledTableCell>
                      <StyledTableCell style={{ width: 160 }} align="right">
                        <Button
                          variant="contained"
                          color="secondary"
                          onClick={() => {
                            handleDelete(item.taiKhoan);
                          }}
                        >
                          Delete
                        </Button>
                      </StyledTableCell>
                      <StyledTableCell style={{ width: 160 }} align="right">
                        <Button
                          variant="contained"
                          color="primary"
                          onClick={() => {
                            handleUpdate(item);
                          }}
                        >
                          Update
                        </Button>
                      </StyledTableCell>
                    </StyledTableRow>
                  ))}

                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[
                      6,
                      10,
                      25,
                      { label: "All", value: -1 },
                    ]}
                    colSpan={6}
                    count={listUser.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      inputProps: { "aria-label": "rows per page" },
                      native: true,
                    }}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </TableContainer>
        </Box>
      </Container>
    </div>
  );
}
